import { Component, OnInit } from '@angular/core';
import { Book } from 'src/Models/Book';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit 
{
  Title: string = "Lolo";

  Books: Book[] = [];

  constructor() 
  {
    let book1 = new Book();
    book1.Id = 1;
    book1.Title = "Lord Of the Rings";

    let book2 = new Book();
    book2.Id = 2;
    book2.Title = "Dune";

    this.Books.push(book1);
    this.Books.push(book2);
    

  }

  ngOnInit(): void {
  }

}

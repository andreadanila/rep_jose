﻿using Library.Lib.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Library.Web.App
{
    public class LibraryDbContextFactory : IDesignTimeDbContextFactory<LibraryDbContext>
    {
        static string DbConnection { get; set; }

        public LibraryDbContext CreateDbContext(string[] args = null)
        {
            var optionsBuilder = new DbContextOptionsBuilder<LibraryDbContext>();

            if (string.IsNullOrWhiteSpace(DbConnection))
            {
                var config = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json")
                  .Build();

                DbConnection = config.GetConnectionString("LibraryMySQLDB");
            }
            optionsBuilder.UseMySql(connectionString: DbConnection,
                new MySqlServerVersion(new Version(8, 0, 23)), b => b.MigrationsAssembly("Library.Web"));

            return new LibraryDbContext(optionsBuilder.Options);
        }
    }
}

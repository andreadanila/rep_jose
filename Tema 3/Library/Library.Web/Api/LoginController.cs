﻿using Library.Lib.DAL;
using Library.Lib.Models;
using Library.Lib.Services;
using Library.Web.App;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Library.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        LibraryDbContext dbContext { get; set; }
        AuthenticationService authSvc { get; set; }

        public LoginController()
        {
            var contextFactory = new LibraryDbContextFactory();
            dbContext = contextFactory.CreateDbContext();

            authSvc = new AuthenticationService(dbContext);
        }

        // GET api/<LoginController>/email/password
        [HttpGet("{email}/{password}")]
        public Member Get(string email, string password)
        {
            return authSvc.Login(email, password);
        }

    }
}

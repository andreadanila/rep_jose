﻿using Library.Lib.DAL;
using Library.Lib.Models;
using Library.Lib.Services;
using Library.Web.App;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Library.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        LibraryDbContext dbContext { get; set; }
        AuthenticationService authSvc { get; set; }

        public RegisterController()
        {
            var contextFactory = new LibraryDbContextFactory();
            dbContext = contextFactory.CreateDbContext();

            authSvc = new AuthenticationService(dbContext);
        }


        // POST api/<RegisterController>
        [HttpPost]
        public RegisterResponse Post([FromBody] RegisterInfo info)
        {
            return authSvc.Register(info);
        }

    }
}

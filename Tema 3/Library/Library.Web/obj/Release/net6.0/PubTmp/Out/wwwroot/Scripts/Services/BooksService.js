class BooksService
{
    constructor($http)
    {
       this.Http = $http;
    }

    Add(item)
    {
        
    }

    Update(item)
    {
       
    }

    GetAll()
    {
        let url = 'api/books';
        return this.Http(
        {
            method: 'GET',            
            url: url     
        });
    }

    Find(id)
    {
        let url = 'api/books/' + id;
        return this.Http(
        {
            method: 'GET',            
            url: url     
        });
    }

    Delete(id)
    {
        
    }
}


App.service('BooksService', BooksService);
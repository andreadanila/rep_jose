class RegisterService
{
    constructor($http)
    {
       this.Http = $http;
    }

    RequestRegister(info)
    {
        let url = 'api/register';

        var postRequest = {
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'application/json' },
            data: info
           };

        return this.Http(postRequest);
           
    }
}

RegisterService.$inject = ['$http'];
App.service('RegisterService', RegisterService);
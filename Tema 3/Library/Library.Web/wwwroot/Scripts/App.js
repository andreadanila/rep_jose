var App = angular.module("PericoUIApp", [
                    'ngRoute',
                    'ui.grid']);

App.config(function($routeProvider, $locationProvider){
    $routeProvider
    .when("/Books", { template: "<books></books>"})    
    .when("/Books/Display/:id", { template: "<bookdisplay></bookdisplay>"})    
    .when("/Books/Edit/:id", { template: "<bookedit></bookedit>"})    
    .when("/Books/Add", { template: "<bookadd></bookadd>"})    
    .when("/Members", { template: "<members></members>"});

    $locationProvider.html5Mode(true);
});


App.ClientGlobals = new ClientGlobals();
class Menu
{
  #location = null;

  constructor($location)
  {
    this.#location = $location;
  }

  NavigateTo(view)
  {
    this.#location.path("/" + view)
  }
}

App.
  component('menu', {   
    templateUrl: 'scripts/views/menu/menu.html',
    controller: Menu,
    controllerAs: "vm"
  });
﻿using Library.Lib.Models;
using Microsoft.EntityFrameworkCore;

namespace Library.Lib.DAL
{
    public class LibraryDbContext : DbContext
    {
        public DbSet<Member> Members { get; set; }
        public DbSet<Book> Books { get; set; }

        public DbSet<BookCopy> BookCopies { get; set; }

        public LibraryDbContext(DbContextOptions<LibraryDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // add your own confguration here
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Member>().ToTable("Members");
            modelBuilder.Entity<Book>().ToTable("Books");
            modelBuilder.Entity<BookCopy>().ToTable("BookCopies");

            modelBuilder.Entity<BookCopy>()
                .HasOne(x => x.ParentBook)
                .WithMany(x => x.BookCopies)
                .HasForeignKey(x => x.ParentBookId);
        }
    }
}
﻿using Library.Lib.DAL;
using Library.Lib.Models;

namespace Library.Lib.Services
{
    public class AuthenticationService
    {
        LibraryDbContext DbContext { get; set; }

        public AuthenticationService(LibraryDbContext dbContext)
        {
            DbContext = dbContext;
        }
        public Member Login(string email, string password)
        {
            var existingUser = DbContext.Members.FirstOrDefault(x => x.Email == email && x.Password == password);
            return existingUser;
        }

        public RegisterResponse Register(RegisterInfo info)
        {
            var output = new RegisterResponse();

            var existingUser = DbContext.Members.FirstOrDefault(x => x.Email == info.Email);

            if (existingUser != null)
            {
                output.IsSuccess = false;
                output.Message = "Ya existe un usuario con ese email";
            }
            else if (string.IsNullOrEmpty(info.Email)
                || string.IsNullOrEmpty(info.Name)
                || string.IsNullOrEmpty(info.Name))
            {
                output.IsSuccess = false;
                output.Message = "Ningún campo puede estar vacío";
            }
            else
            {
                var member = new Member();
                member.Id = Guid.NewGuid();
                member.Email = info.Email;
                member.Name = info.Name;
                member.Password = info.Password;

                DbContext.Members.Add(member);
                DbContext.SaveChanges();

                output.Member = member;
            }

            return output;
        }
    }
}
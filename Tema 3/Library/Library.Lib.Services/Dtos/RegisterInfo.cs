﻿namespace Library.Lib.Services
{
    public class RegisterInfo
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
    }
}

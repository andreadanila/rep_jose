﻿using Library.Lib.Models;

namespace Library.Lib.Services
{
    public class RegisterResponse
    {
        public Member Member { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }
    }
}

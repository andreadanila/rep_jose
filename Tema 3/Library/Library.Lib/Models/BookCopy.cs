﻿using Common.Lib.Core;
using System;

namespace Library.Lib.Models
{
    public class BookCopy : Entity
    {
        public string Code { get; set; }

        public Guid ParentBookId { get; set; }
        public Book ParentBook { get; set; }
    }
}

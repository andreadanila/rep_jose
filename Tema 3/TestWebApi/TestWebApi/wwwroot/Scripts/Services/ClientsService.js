class ClientsService
{
    Http = null;

    constructor($http)
    {
        this.Http = $http;
    }

    // GET: api/<ClientsController>
    GetAll()
    {
        return this.Http(
            {
                method: 'GET',
                url: 'api/clients'
            });
    }

    // GET api/<ClientsController>/5
    Find(id) {

    }

    // POST api/<ClientsController>
    Add(client) {

    }

    // PUT api/<ClientsController>
    Update(client) {

    }

    // DELETE api/<ClientsController>/5
    Delete(id) {

    }
}


ClientsService.$inject = ['$http'];
LibraryApp.service('ClientsService', ClientsService);

class LoginService
{
    constructor($http)
    {
       this.Http = $http;
    }

    RequestLogin(email, password)
    {
        let url = 'api/login/' + email + "/" + password;
        return this.Http(
        {
            method: 'GET',            
            url: url     
        });
           
    }
}

LoginService.$inject = ['$http'];
App.service('LoginService', LoginService);
class BookDisplay
{
  Code = "";
  Title = "";
  Author = "";
  Year = "";

  constructor($routeParams, booksService)
  {
    var id = $routeParams.id;

    booksService.Find(id).then(
      (item)=> alert(item.Title)), 
      (error)=> alert(error);
  }

}

BookDisplay.$inject = ['$routeParams', 'BooksService'];

App.
  component('bookdisplay', {   
    templateUrl: 'Scripts/Views/NavigationViews/BookViews/BookDisplay/bookdisplay.html',
    controller: BookDisplay,
    controllerAs: "vm"
  });
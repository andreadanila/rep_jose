var App = angular.module("SampleUIApp", ['ngRoute', 'ui.grid']);

App.config(function($routeProvider, $locationProvider){
    $routeProvider
    .when("/Books", { template: "<books></books>"})    
    .when("/Books/Display/:id", { template: "<bookdisplay></bookdisplay>"})    
    .when("/Members", { template: "<members></members>"})
    ;

    $locationProvider.html5Mode(true);
});

App.ClientGlobals = new ClientGlobals();
class BooksService
{
    #http = null;
    static Items = new Array();

    constructor($http)
    {
        this.#http = $http;

        if(BooksService.Items.length == 0)
        {
            var book1 = new Book();
            book1.Id = "aasdfsadf456353sfasdfasdfasdf";
            book1.Code = "a1";
            book1.Title = "The Lord of the rings";
            book1.Author = "JRR Tolkien";
            book1.Year = 1954;

            var book2 = new Book();
            book2.Id = "ahhfdghdhdv55464642";
            book2.Code = "a2";
            book2.Title = "Dune";
            book2.Author = "Frank Herbert";
            book2.Year = 1965;

            BooksService.Items.push(book1);
            BooksService.Items.push(book2);
        }
    }

    GetAll()
    {
        /*var getReq = 
        {
            method: 'GET',            
            url: 'api/spaceships'            
        };

        this.Http(getReq).then(
            (response) => this.LoadShips(response.data.$values), // on success
            (error) => alert(error.statusText));                   // on error*/

        let myPromise = new Promise(function(myResolve, myReject) 
        {
            myResolve(BooksService.Items);
        });

        return myPromise;
    }

    Find(id)
    {
        let myPromise = new Promise(function(myResolve, myReject) 
        {
            for(var i in BooksService.Items)
            {
                var item = BooksService.Items[i];
                if(item.Id == id)
                    myResolve(item); 

                return;
            }
        });

        return myPromise;

        
    }
}


BooksService.$inject = ['$http'];

App.service('BooksService', BooksService);